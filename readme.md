## SDA Avdanced Java - Chess Game 
##### Opis projektu
Projekt ma za zadanie zasymulować faktyczne warunki pracy nad rozwojem aplikacji. 
W projekcie jest gotowy Frontend pod który należy dopisać obsługę backendową.
Komunikacja między backendem a frontendem opiera się na websocketach, 
które również zostały wstępnie skonfigurowane. Frontend nieustannie nasłuchuje 
endpoint, na który należy wysłać dane o grze w formacie JSON o następującej strukturze:

```
{  
"chessboard" : {
  "tiles" : [ [ {  
   "color" : "WHITE",  
   "name" : "A1",  
   "piece" : {  
    "color" : "WHITE",  
    "symbol" : "R"  
   },  
   "selected" : false,  
   "available" : false  
   }, {  
   "color" : "BLACK",  
   "name" : "B1",  
   "piece" : {  
    "color" : "WHITE",  
    "symbol" : "N"  
   },  
   "selected" : false,  
   "available" : false  
   }  
   ...  
  ] ]
}  
```
_patrz plik json.txt_

##### Zadania do wykonania
1. Utwórz GameState
2. Utwórz Chessboard wraz z Tile
3. Utwórz Figury
 


