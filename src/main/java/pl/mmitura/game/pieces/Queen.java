package pl.mmitura.game.pieces;

import lombok.Getter;
import lombok.Setter;
import pl.mmitura.game.Color;
import pl.mmitura.game.pieces.moves.Move;

import java.util.List;

@Getter
@Setter
public class Queen extends Piece {

    String symbol = "Q";
    Color color;

    public Queen(Color color) {
        this.color = color;
    }

    @Override
    public List<Move> getMoves() {
        return null;
    }
}
