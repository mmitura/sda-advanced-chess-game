package pl.mmitura.game.pieces;

import lombok.Getter;
import pl.mmitura.game.Color;
import pl.mmitura.game.pieces.moves.Move;

import java.util.List;

@Getter
public abstract class Piece {

    // Klasa abstrakcyjna dla figur
    // Każda figura powinna zawierać jednoznakowy String symbol oraz ? kolor (wymagania z frontendu, aby móc reprezentować figury)
    Color color;

    public abstract List<Move> getMoves();

}
