package pl.mmitura.game.pieces;

import lombok.Getter;
import lombok.Setter;
import pl.mmitura.game.Color;
import pl.mmitura.game.pieces.moves.Move;

import java.util.List;

@Getter
@Setter
public class King extends Piece {


    String symbol = "K";
    Color color;

    public King(Color color) {
        this.color = color;
    }

    @Override
    public List<Move> getMoves() {
        return null;
    }
}
