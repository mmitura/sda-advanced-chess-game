package pl.mmitura.game.pieces;

import lombok.Getter;
import lombok.Setter;
import pl.mmitura.game.Color;
import pl.mmitura.game.chessboard.Tile;
import pl.mmitura.game.pieces.moves.Move;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class Pawn extends Piece {

    String symbol = "P";
    Color color;

    public Pawn(Color color) {
        this.color = color;
    }

    @Override
    public List<Move> getMoves() {
        return null;
    }
}
