package pl.mmitura.game.pieces;

import lombok.Getter;
import lombok.Setter;
import pl.mmitura.game.Color;
import pl.mmitura.game.pieces.moves.Move;

import java.util.List;

@Getter
@Setter
public class Knight extends Piece {

    String symbol = "N";
    Color color;

    public Knight(Color color) {
        this.color = color;
    }

    @Override
    public List<Move> getMoves() {
        return null;
    }
}
