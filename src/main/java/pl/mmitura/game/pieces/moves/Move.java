package pl.mmitura.game.pieces.moves;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Move {
    String startingTile;
    String destinationTile;
}
