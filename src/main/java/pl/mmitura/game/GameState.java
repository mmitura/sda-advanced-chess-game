package pl.mmitura.game;

import lombok.Getter;
import lombok.Setter;
import pl.mmitura.game.chessboard.Chessboard;

import static pl.mmitura.game.Color.*;

@Setter
@Getter
public class GameState {

    // Ta klasa powinna zawierać informacje o grze
    // Sugerowane pola: Chessboard chessboard oraz ? tura
    Chessboard chessboard;
    Color turn;

    private GameState() {
    }

    public static GameState createNewGame() {
        GameState gameState = new GameState();
        gameState.setChessboard(Chessboard.generateChessboard());
        gameState.setTurn(WHITE);
        return gameState;
    }
}
