package pl.mmitura.game.chessboard;

import lombok.Getter;
import lombok.Setter;
import pl.mmitura.game.Color;
import pl.mmitura.game.pieces.*;

import java.util.Arrays;

import static pl.mmitura.game.Color.BLACK;
import static pl.mmitura.game.Color.WHITE;

@Getter
@Setter
public class Chessboard {

    // Klasa przedstawiajaca szachownice
    // Powinna zawierać dwuwymiarową tablicę przechowującą klasę Tile o nazwie tiles (front interuje po tiles)
    private Tile[][] tiles = new Tile[8][8];

    private Chessboard() {
    }

    public static Chessboard generateChessboard() {
        Chessboard chessboard = new Chessboard();
        chessboard.fillWithTiles();
        chessboard.fillChessboardWithPieces();
        return chessboard;
    }

    private void fillWithTiles() {
        for (int i = 0; i < tiles.length ; i++) {
            for (int j = 0; j < tiles[i].length; j++) {
                tiles[i][j] = Tile.builder()
                        .available(false)
                        .selected(false)
                        .piece(null)
                        .name(Tile.generateName(i, j))
                        .color((i + j) % 2 == 0 ? BLACK : WHITE)
                        .build();
            }
        }
    }

    private void fillChessboardWithPieces() {
        fillWithOlderFigures(tiles[0], WHITE);
        fillWithPawns(tiles[1], WHITE);
        fillWithPawns(tiles[6], BLACK);
        fillWithOlderFigures(tiles[7], BLACK);
    }

    private void fillWithPawns(Tile[] row, Color color) {
        Arrays.stream(row)
                .forEach(tile -> {
                    Pawn piece = new Pawn(color);
                    tile.setPiece(piece);
                });
    }

    private void fillWithOlderFigures(Tile[] row, Color color) {
        row[0].setPiece(new Rook(color));
        row[1].setPiece(new Knight(color));
        row[2].setPiece(new Bishop(color));
        row[3].setPiece(new Queen(color));
        row[4].setPiece(new King(color));
        row[5].setPiece(new Bishop(color));
        row[6].setPiece(new Knight(color));
        row[7].setPiece(new Rook(color));
    }

    public Tile getTile(String name) {
        return Arrays.stream(tiles)
                .flatMap(row -> Arrays.stream(row))
                .filter(tile -> tile.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Selected tile " + name + " cannot be found on chessboard"));
    }
}
