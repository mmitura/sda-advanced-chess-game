package pl.mmitura.game.chessboard;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.mmitura.game.Color;
import pl.mmitura.game.pieces.Piece;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Builder
@Getter
@Setter
public class Tile {

    // Klasa dla pól szachownicy
    // Wymagania z frontu:
    // Klasa musi zawierać String name, aby było wiadomo o które pole na froncie chodzi
    // Klasa musi zawierać boolean selected, aby było wiadomo że aktuale pole jest zaznaczone
    // Klasa musi zawierać boolean avaliable, aby było wiadomo, że na dane pole można wykonać ruch
    // Klasa musi zawierać Piece piece, aby było wiadomo, jaka figura znajduje się na danym polu
    // Pytanie co jeszcze powinna zawierać ta klasa?
    private String name;
    private boolean selected, available;
    private Piece piece;
    private Color color;

    private static final Map<Integer, String> numbersWithColumn = Stream.of(
            new AbstractMap.SimpleEntry<>(0, "A"),
            new AbstractMap.SimpleEntry<>(1, "B"),
            new AbstractMap.SimpleEntry<>(2, "C"),
            new AbstractMap.SimpleEntry<>(3, "D"),
            new AbstractMap.SimpleEntry<>(4, "E"),
            new AbstractMap.SimpleEntry<>(5, "F"),
            new AbstractMap.SimpleEntry<>(6, "G"),
            new AbstractMap.SimpleEntry<>(7, "H"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));


    public static String generateName(int i, int j) {
        String row = String.valueOf(i + 1);
        String column = numbersWithColumn.get(j);
        return column + row;
    }
}

