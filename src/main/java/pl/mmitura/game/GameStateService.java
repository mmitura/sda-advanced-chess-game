package pl.mmitura.game;

import org.springframework.stereotype.Service;
import pl.mmitura.game.chessboard.Tile;
import pl.mmitura.game.pieces.Piece;
import pl.mmitura.websocket.SelectedTile;

@Service
public class GameStateService {

    public void handle(SelectedTile selectedTile, GameState gameState) {
        Tile tile = gameState.getChessboard().getTile(selectedTile.getName());
        Piece piece = tile.getPiece();
        if (piece != null) {
            boolean selectedTileContainsPieceOfActivePlayer = piece.getColor().equals(gameState.getTurn());
            if (selectedTileContainsPieceOfActivePlayer) {
                tile.setSelected(true);
                piece.getMoves();
            }
        }
    }

}
