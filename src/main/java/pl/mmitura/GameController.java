package pl.mmitura;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import pl.mmitura.game.GameState;
import pl.mmitura.game.GameStateService;
import pl.mmitura.websocket.SelectedTile;

import java.util.Map;


@Controller
public class GameController {

    // W tej metodzie określamy co ma się stać, gdy front poprosi o wygenerowanie nowej gry
    @MessageMapping("/newgame")
    @SendTo("/topic/board")
    public GameState newGame(SimpMessageHeaderAccessor headerAccessor) throws Exception {
        // W tym miejscu powinnismy utworzyc nowy gamestate, możeby użyć jakiegoś wzorca projektowego?
        GameState gameState = GameState.createNewGame();

        // Zdefiniowana metoda umieszczająca nowoutworzony gamestate w sesji
        storeGamestateInSession(headerAccessor, gameState);

        return gameState;
    }

    private void storeGamestateInSession(SimpMessageHeaderAccessor headerAccessor, GameState gameState) {
        String sessionId = headerAccessor.getSessionId();
        headerAccessor.getSessionAttributes().put("gamestate", gameState);
        System.out.println("New game created, sessionId: " + sessionId);
    }

    // W ten metodzie określamy co ma się stać, gdy na froncie gracz zaznaczy pole
    @MessageMapping("/move")
    @SendTo("/topic/board")
    public GameState move(SelectedTile selectedTile, SimpMessageHeaderAccessor headerAccessor) throws Exception {
        Map<String, Object> attributesMap = headerAccessor.getSessionAttributes();
        String sessionId = headerAccessor.getSessionId();
        if (attributesMap.containsKey("gamestate")) {
            GameState gameState = (GameState) attributesMap.get("gamestate");
            GameStateService gameStateService = new GameStateService();
            gameStateService.handle(selectedTile, gameState);
            // W tym miejscu pobraliśmy gamestate z sesji, powinniśmy jakoś go przeanalizować na podstawie pola które zaznaczył gracz (selectedTile)
            System.out.println("Active sessionId: " + sessionId + " selected tile: " + selectedTile.getName());

            return gameState;
        }
        return null;
    }
}