package pl.mmitura.websocket;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SelectedTile {
    // Klasa jest reprezentacją JSON'a z frontu
    public String name;

}
