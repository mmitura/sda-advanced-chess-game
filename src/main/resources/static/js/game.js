// deklaracja zmiennych
var stompClient = null
var connected = false

// odpalenie funkcji connect tuż po załadowaniu strony
connect()

// funkcja connect -> nawiązuje połączenie websocketowe z aplikacją
function connect() {
    var socket = new SockJS('/game')
    stompClient = Stomp.over(socket)
    stompClient.connect({}, function(frame) {
        console.log('Connected')
        stompClient.subscribe('/topic/board', function(gameState) {
            // konfiguracja co ma się stać gdy przyjdzie odpowiedź z serwera na endpoint board
            update(JSON.parse(gameState.body))
        });
        // po nawiązaniu połączenia uruchamiamy startGame()
        startGame()
    });
    connected = true;
}

// funkcja startGame -> wysyła na endpoint newgame pustego jsona
function startGame() {
    if (connected && stompClient !== null) {
        stompClient.send("/app/newgame", {}, JSON.stringify({}))
        console.log('Requested a start of new game')
    }
}

// funkcja selectTile -> wysyła na endpoint move jsona zawierającego informację o wybranej kratce
function selectTile(name) {
    stompClient.send("/app/move", {}, JSON.stringify({ 'name': name }))
    console.log('Selected tile: ' + name)
}

// funkcja update -> iteruje po otrzymanym
function update(gamestate) {
    for (var row of gamestate.chessboard.tiles) {
        for(var tile of row) {
            var element = document.getElementById(tile.name);
            if (tile.selected) {
                element.classList.add("selected")
            } else {
                if (element.classList.contains("selected")) {
                    element.classList.remove("selected")
                }
            }
            if (tile.available) {
                element.classList.add("available")
            } else {
                if (element.classList.contains("available")) {
                    element.classList.remove("available")
                }
            }

            if (tile.piece != null) {
                element.innerHTML = '<img src="img/' + tile.piece.color.substring(0, 1) + tile.piece.symbol + '.png">'
            } else {
                element.innerHTML = ''
            }
        }
    }
}